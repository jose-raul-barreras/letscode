 /* file minunit_example.c */
#include <stdio.h>

#include "minunit.h"
#include "../candles.h"

int tests_run = 0;

static char * test_first_higher() {
    int n = 5;
    int heigths[5] = {19, 10, 8, 17, 9};
    int res = candles_to_blow(n, heigths);
    mu_assert("error, first_higher", res == 1);
    return 0;
}

static char * test_all_equals() {
    int n = 5;
    int heigths[5] = {19, 10, 19, 19, 19};
    int res = candles_to_blow(n, heigths);
    mu_assert("error testing all equals", res == 5);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_first_higher);
    mu_run_test(test_all_equals);
    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);

    return result != 0;
}
